//A normal map fragment shader

#version 410

in vec4 vPosition;
in vec3 vNormal;
in vec2 vTexCoord;
in vec3 vTangent;
in vec3 vBiTangent;

uniform vec3 cameraPosition;

uniform vec3 AmbientColour;
uniform vec3 LightColour;
uniform vec3 LightDirection;
uniform vec3 Ka; //ambient material colour
uniform vec3 Kd; //diffuse material colour
uniform vec3 Ks; //specular material colour
uniform float specularPower; //material specular power

uniform sampler2D diffuseTexture;
uniform sampler2D specularTexture;
uniform sampler2D normalTexture;

const int MAX_LIGHTS = 4;	//max number of lights affecting the object being drawn this draw call
uniform int numLights;
uniform vec3 PointLightColour[MAX_LIGHTS];
uniform vec3 PointLightPosition[MAX_LIGHTS];

out vec4 FragColour;

//calculate light colour * lambert term
vec3 diffuse(vec3 direction, vec3 colour, vec3 normal)
{
	return colour * max( 0, dot( normal, -direction));
}

//calculate specular light from a given light source
vec3 specular(vec3 direction, vec3 colour, vec3 normal, vec3 view)
{
	vec3 R = reflect( direction, normal );	//reflection per light
	float specularTerm = pow( max( 0, dot( R, view ) ), specularPower );
	return specularTerm * colour;
}

void main()
{
	//ensure normal and light direction are normalized
	vec3 N = normalize(vNormal);
	vec3 T = normalize(vTangent);
	vec3 B = normalize(vBiTangent);
	vec3 L = normalize(LightDirection);

	mat3 TBN = mat3(T, B, N); //construct tangent basis matrix

	vec3 texDiffuse = texture( diffuseTexture, vTexCoord ).rgb;
	vec3 texSpecular = texture( specularTexture, vTexCoord ).rgb;
	vec3 texNormal = texture( normalTexture, vTexCoord ).rgb;

	N = TBN * (texNormal * 2 - 1);

	//calculate diffuse lighting from sunlight
	vec3 diffuseTotal = diffuse(L, LightColour, N);

	//calculate view vector and reflection vector
	vec3 V = normalize(cameraPosition - vPosition.xyz);
	vec3 R = reflect( L, N );

	//calculate specular light from directional light
	vec3 specularTotal = specular(L, LightColour, N, V);

	//calculate specular term
	float specularTerm = pow( max( 0, dot( R, V ) ), specularPower );

	//process point lights
	for (int i = 0; i < numLights; i++)
	{
		vec3 direction = vPosition.xyz - PointLightPosition[i];
		float distance = length(direction);
		direction = direction / distance;

		//attenuate the light intensity with inverse square law
		vec3 colour = PointLightColour[i] / (distance * distance);

		diffuseTotal += diffuse(direction, colour, N);
		specularTotal += specular(direction, colour, N, V);
	}

	//calculate each colour property
	vec3 ambient = AmbientColour * Ka * texDiffuse;
	vec3 diffuse = Kd * texDiffuse * diffuseTotal;
	vec3 specular = Ks * texSpecular * specularTotal;

	//output lambert as greyscale
	FragColour = vec4( ambient + diffuse + specular, 1 );
	//FragColour = vec4( specular, 1 );
	//FragColour = vec4(N, 1);
}