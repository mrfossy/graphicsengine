#include "Mesh.h"
#include <gl_core_4_4.h>

Mesh::~Mesh()
{
	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);
	glDeleteBuffers(1, &ibo);
	//If any value is zero, these calls do nothing (like calling delete on a null pointer)
}

void Mesh::initializeQuad()
{
	//Check that the mesh is not already initialized
	assert(vao == 0);
	
	//Generate buffers
	glGenBuffers(1, &vbo);
	glGenVertexArrays(1, &vao);

	//Binding marks something as 'active' in OpenGL (either active for editing or active for drawing)
	//Bind vertex array (aka a mesh wrapper)
	glBindVertexArray(vao);

	//When you bind a VBO it 'sticks' to the currently bound VAO; so we bind VAO first, then VBO
	//Bind vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	//Define 6 vertices for 2 triangles (positions defined in counter-clockwise order)
	Vertex vertices[6];
	vertices[0].position = { -0.5f, 0, 0.5f, 1 };
	vertices[1].position = { 0.5f, 0, 0.5f, 1 };
	vertices[2].position = { -0.5f, 0, -0.5f, 1 };

	vertices[3].position = { -0.5f, 0, -0.5f, 1 };
	vertices[4].position = { 0.5f, 0, 0.5f, 1 };
	vertices[5].position = { 0.5f, 0, -0.5f, 1 };
	
	//Set all six vertices to face 'up'
	vertices[0].normal = { 0, 1, 0, 0 };
	vertices[1].normal = { 0, 1, 0, 0 };
	vertices[2].normal = { 0, 1, 0, 0 };
	vertices[3].normal = { 0, 1, 0, 0 };
	vertices[4].normal = { 0, 1, 0, 0 };
	vertices[5].normal = { 0, 1, 0, 0 };

	//Set the texture coordinates
	vertices[0].texCoord = { 0, 1 };	//bottom left
	vertices[1].texCoord = { 1, 1 };	//bottom right
	vertices[2].texCoord = { 0, 0 };	//top left
	vertices[3].texCoord = { 0, 0 };	//top left
	vertices[4].texCoord = { 1, 1 };	//bottom right
	vertices[5].texCoord = { 1, 0 };	//top right

	//Fill out VBO with vertex data
	//VBO is currently bounding due to glBindBuffer() call, so use glBufferData() to assign data
	//Four paramaters:
	//- GL_ARRAY_BUFFER is the type buffer you're using
	//- size of data being used (6* vertices)
	//- pointer to data
	//- OpenGL constant to define if the data will update after it is defined (you won't be changing data, so use GL_STATIC_DRAW)
	glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(Vertex), vertices, GL_STATIC_DRAW);

	//Enable 1st attribute (position), passing in a zero-indexed integer to specify which attribute you're enabling
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);

	//Enable the 2nd attribute (normals)
	glEnableVertexAttribArray(1);
	//GL_TRUE automatically normalizes the vector when it is transferred to the GPU
	//Final parameter is how many bytes into the Vertex struct is the normal property (just after the first vec4, i.e. 16 bytes)
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_TRUE, sizeof(Vertex), (void*)16);

	//Enable the 3rd attribute (texture coordinates)
	glEnableVertexAttribArray(2);
	//texCoord is the third element in the struct; 8 floats from the start of the struct; each float is 4 bytes, so texCoord is 32 bytes in
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)32);

	//Unbind buffers
	//This is a safety measure; you don't want to accidentally modify the data since OpenGL
	//works on currently bound information. Future calls to GL API methods will use whatever is left bound.
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Quad has two triangles
	triCount = 2;
}

void Mesh::initialize(unsigned int vertexCount, const Vertex* vertices, unsigned int indexCount, unsigned int* indices)
{
	//Check that the mesh is not already initialized
	assert(vao == 0);

	//Generate buffers
	glGenBuffers(1, &vbo);
	glGenVertexArrays(1, &vao);

	//Bind vertex array (aka mesh wrapper)
	glBindVertexArray(vao);

	//Bind vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	//Fill vertex buffer
	glBufferData(GL_ARRAY_BUFFER, vertexCount * sizeof(Vertex), vertices, GL_STATIC_DRAW);

	//Enable first element as position
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);

	//Enable second element as normal
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_TRUE, sizeof(Vertex), (void*)16);

	//Enable the third element as texture
	glEnableVertexAttribArray(2);
	//texCoord is the third element in the struct; 8 floats from the start of the struct; each float is 4 bytes, so texCoord is 32 bytes in
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)32);

	//Bind indices if there are any
	if (indexCount != 0)
	{
		//Generate the index buffer object
		glGenBuffers(1, &ibo);

		//Bind the ibo to the GL_ELEMENT_ARRAY_BUFFER, which represents index buffers
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

		//Fill vertex buffer; bind the index buffer data
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexCount * sizeof(unsigned int), indices, GL_STATIC_DRAW);

		triCount = indexCount / 3;
	}
	else
	{
		triCount = vertexCount / 3;
	}

	//Unbind buffers
	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Mesh::draw()
{
	//To draw, you need to rebind your VAO; you don't need to bind anything else
	//since it was all bound to your VAO when you initialized your data
	
	glBindVertexArray(vao);
	//Using indices or just vertices? (future proofing)
	if (ibo != 0)
	{
		//glDrawElements() refers to using an index buffer
		glDrawElements(GL_TRIANGLES, 3 * triCount, GL_UNSIGNED_INT, 0);
	}
	else
	{
		//glDrawArrays() draws a mesh using just vertices
		glDrawArrays(GL_TRIANGLES, 0, 3 * triCount);
	}
}