#include "Light.h"

Light::Light()
{
	m_direction = { 0, 0, 0 };
	m_colour = { 1, 1, 1 };
}

//Constructor for directional lights (i.e. sunlight)
Light::Light(glm::vec3 direction, glm::vec3 colour)
{
	m_direction = direction;
	m_colour = colour;
}

//Constructor for point lights
Light::Light(glm::vec3 position, glm::vec3 colour, float intensity)
{
	m_direction = position;
	m_colour = colour * intensity;
}