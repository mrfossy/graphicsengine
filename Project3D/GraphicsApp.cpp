#include "GraphicsApp.h"
#include "Gizmos.h"
#include "Input.h"
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <iostream>
#include <imgui.h>
#include "Scene.h"

using glm::vec3;
using glm::vec4;
using glm::mat4;
using aie::Gizmos;

GraphicsApp::GraphicsApp()
{
	m_sunlightColour = { 0, 0, 0 };
	m_sunlightDirection = { 0, 0, 0 };
	m_pointLightOneColour = { 0, 0, 0 };
	m_pointLightOnePosition = { 0, 0, 0 };
	m_pointLightOneIntensity = 0.0f;
	m_pointLightTwoColour = { 0, 0, 0 };
	m_pointLightTwoPosition = { 0, 0, 0 };
	m_pointLightTwoIntensity = 0.0f;
	m_pointLightThreeColour = { 0, 0, 0 };
	m_pointLightThreePosition = { 0, 0, 0 };
	m_pointLightThreeIntensity = 0.0f;
	m_scene = nullptr;
}

GraphicsApp::~GraphicsApp()
{
}

bool GraphicsApp::startup()
{
	setBackgroundColour(0.25f, 0.25f, 0.25f);

	//Initialise gizmo primitive counts
	Gizmos::create(10000, 10000, 10000, 10000);

	//Load normal map vertex and fragment shaders
	m_normalMapShader.loadShader(aie::eShaderStage::VERTEX, "./shaders/normalmap.vert");
	m_normalMapShader.loadShader(aie::eShaderStage::FRAGMENT, "./shaders/normalmap.frag");

	if (m_normalMapShader.link() == false)
	{
		printf("Shader Error: %s\n", m_normalMapShader.getLastError());
		return false;
	}

	//Load fire hydrant mesh
	//Author: Jakub Królikowski
	//https://sketchfab.com/3d-models/yellow-fire-hydrant-ad3b24b0d191461dbc850059987c8262
	//Licence: https://creativecommons.org/licenses/by/4.0/
	if (m_fireHydrantMesh.load("./models/firehydrant/firehydrant.obj", true, true) == false)
	{
		printf("Fire hydrant mesh error!\n");
		return false;
	}

	//Create fire hydrant transform
	glm::mat4 fireHydrantTransform = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	};

	//Load brick wall mesh
	//Author: Philipp Busse
	//https://sketchfab.com/3d-models/damaged-wall-11616746d0ea4bd59a2d9cf9a2b2fdfa
	//Licence: https://creativecommons.org/licenses/by/4.0/
	if (m_wallMesh.load("./models/wall/wall.obj", true, true) == false)
	{
		printf("Wall mesh error!\n");
		return false;
	}

	//Create brick wall transform
	glm::mat4 wallTransform = {
	1, 0, 0, 0,
	0, 1, 0, 0,
	0, 0, 1, 0,
	0, 0, 0, 1
	};

	//Set sunlight (directional) colour and direction and ambient light colour
	m_sunlightColour = { 1, 1, 1 };		//white
	m_sunlightDirection = { 1, -1, 1 };
	m_sunlight.setColour(m_sunlightColour);
	m_sunlight.setDirection(m_sunlightDirection);
	glm::vec3 ambientLight = { 0.25f, 0.25f, 0.25f };		//dark grey

	//Create a scene; add fire hydrant and brick wall instances
	m_scene = new Scene(&m_camera, glm::vec2(getWindowWidth(), getWindowHeight()), &m_sunlight, ambientLight);
	m_scene->addInstance(new Instance(fireHydrantTransform, &m_fireHydrantMesh, &m_normalMapShader, { 0, 1.5, 0 }, { 0, 0, 0 }, { 6.0, 6.0, 6.0 }));
	m_scene->addInstance(new Instance(wallTransform, &m_wallMesh, &m_normalMapShader, { 0, 0, 5 }, { 0, 0, 0 }, { 0.15, 0.15, 0.15 }));

	//Set point light colours and positions; add to scene
	m_pointLightOneColour = { 1, 0, 0 };
	m_pointLightOnePosition = { 6, 3, 2.5 };
	m_pointLightOneIntensity = 20.0f;
	m_pointLightTwoColour = { 0, 1, 0 };
	m_pointLightTwoPosition = { -6, 3, 2.5 };
	m_pointLightTwoIntensity = 20.0f;
	m_pointLightThreeColour = { 0, 0, 1 };
	m_pointLightThreePosition = { 0, 8, 2.5 };
	m_pointLightThreeIntensity = 20.0f;
	m_scene->addPointLight(m_pointLightOnePosition, m_pointLightOneColour, m_pointLightOneIntensity);	//red light on left
	m_scene->addPointLight(m_pointLightTwoPosition, m_pointLightTwoColour, m_pointLightTwoIntensity);	//green light on right
	m_scene->addPointLight(m_pointLightThreePosition, m_pointLightThreeColour, m_pointLightThreeIntensity);	//blue light in middle

	return true;
}

void GraphicsApp::shutdown()
{
	Gizmos::destroy();
	delete m_scene;
}

void GraphicsApp::update(float deltaTime)
{
	//Query time since application started
	float time = getTime();

	//Update the camera
	m_camera.update(deltaTime);						   

	//Wipe the gizmos clean for this frame
	Gizmos::clear();

	//Draw a 20x20 grid
	vec4 white(1);
	vec4 black(0, 0, 0, 1);
	for (int i = 0; i < 41; ++i) {
		Gizmos::addLine(vec3(-20 + i, 0, 20),
						vec3(-20 + i, 0, -20),
						i == 20 ? white : black);
		Gizmos::addLine(vec3(20, 0, -20 + i),
						vec3(-20, 0, -20 + i),
						i == 20 ? white : black);
	}

	// Add a transform so that we can see the axis
	//Gizmos::addTransform(mat4(16));
	
	//Add spheres to represent point light positions/colours
	Gizmos::addSphere(m_pointLightOnePosition, 0.2, 16, 16, vec4(m_pointLightOneColour.r, m_pointLightOneColour.g, m_pointLightOneColour.b, 0.75f));
	Gizmos::addSphere(m_pointLightTwoPosition, 0.2, 16, 16, vec4(m_pointLightTwoColour.r, m_pointLightTwoColour.g, m_pointLightTwoColour.b, 0.75f));
	Gizmos::addSphere(m_pointLightThreePosition, 0.2, 16, 16, vec4(m_pointLightThreeColour.r, m_pointLightThreeColour.g, m_pointLightThreeColour.b, 0.75f));

	//Quit if we press escape
	aie::Input* input = aie::Input::getInstance();

	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();

	//Set up ImGUI for runtime parameter controls
	ImGui::Begin("Light Settings");	//creates floating window with a given title; you can have an many of these as you'd like
	ImGui::DragFloat3("Sunlight Direction", &m_sunlightDirection[0], 0.01f, -1.0f, 1.0f);	//adds controls for three floats that are consecutive in memory (like a vec3)
	ImGui::DragFloat3("Sunlight Colour", &m_sunlightColour[0], 0.01f, 0.0f, 2.0f);			//takes in a label, sets a drag speed, and sets min and max values
	ImGui::DragFloat3("Point Light 1 Position", &m_pointLightOnePosition[0], 0.1f, -10.0f, 10.0f);	//adds controls for three floats that are consecutive in memory (like a vec3)
	ImGui::DragFloat3("Point Light 1 Colour", &m_pointLightOneColour[0], 0.01f, 0.0f, 2.0f);			//takes in a label, sets a drag speed, and sets min and max values
	ImGui::DragFloat3("Point Light 2 Position", &m_pointLightTwoPosition[0], 0.1f, -10.0f, 10.0f);	//adds controls for three floats that are consecutive in memory (like a vec3)
	ImGui::DragFloat3("Point Light 2 Colour", &m_pointLightTwoColour[0], 0.01f, 0.0f, 2.0f);			//takes in a label, sets a drag speed, and sets min and max values
	ImGui::DragFloat3("Point Light 3 Position", &m_pointLightThreePosition[0], 0.1f, -10.0f, 10.0f);	//adds controls for three floats that are consecutive in memory (like a vec3)
	ImGui::DragFloat3("Point Light 3 Colour", &m_pointLightThreeColour[0], 0.01f, 0.0f, 2.0f);			//takes in a label, sets a drag speed, and sets min and max values

	//Activate special light modes
	ImGui::Checkbox("Dario Argento Mode", &m_argentoLightsMode);
	ImGui::Checkbox("Aurora Mode", &m_auroraMode);
	ImGui::Checkbox("Fairy Lights Mode", &m_fairyMode);

	//Reset all lights
	if (ImGui::Button("Reset Lights"))
	{
		m_sunlightColour = { 1, 1, 1 };
		m_sunlightDirection = { 1, -1, 1 };
		m_pointLightOneColour = { 1, 0, 0 };
		m_pointLightOnePosition = { 6, 3, 2.5 };
		m_pointLightTwoColour = { 0, 1, 0 };
		m_pointLightTwoPosition = { -6, 3, 2.5 };
		m_pointLightThreeColour = { 0, 0, 1 };
		m_pointLightThreePosition = { 0, 8, 2.5 };
		m_argentoLightsMode = false;
		m_auroraMode = false;
		m_fairyMode = false;
		m_pointLightsMoving = false;
	}

	ImGui::DragFloat("Time Modifier", &m_timeModifier, 0.01f, 1.0f, 4.0f);
	
	ImGui::End();	//finalizes the window

	//Update sunlight and point light positions/colours
	m_sunlight.setDirection(m_sunlightDirection);
	m_sunlight.setColour(m_sunlightColour);
	m_scene->getPointLights()[0]->setColour(m_pointLightOneColour);
	m_scene->getPointLights()[0]->setDirection(m_pointLightOnePosition);
	m_scene->getPointLights()[1]->setColour(m_pointLightTwoColour);
	m_scene->getPointLights()[1]->setDirection(m_pointLightTwoPosition);
	m_scene->getPointLights()[2]->setColour(m_pointLightThreeColour);
	m_scene->getPointLights()[2]->setDirection(m_pointLightThreePosition);

	//Cycle point light colours (crazily, but not randomly!)
	if (m_argentoLightsMode)
	{
		m_pointLightOneColour.r = glm::sin(time * 1.60f * m_timeModifier) + 1.0f;
		m_pointLightOneColour.g = glm::sin(time * 1.85f * m_timeModifier) + 1.0f;
		m_pointLightOneColour.b = glm::sin(time * 1.35f * m_timeModifier) + 1.0f;

		m_pointLightTwoColour.r = glm::sin(time * 2.50f * m_timeModifier) + 1.0f;
		m_pointLightTwoColour.g = glm::sin(time * 2.83f * m_timeModifier) + 1.0f;
		m_pointLightTwoColour.b = glm::sin(time * 2.42f * m_timeModifier) + 1.0f;

		m_pointLightThreeColour.r = glm::sin(time * 2.86f * m_timeModifier) + 1.0f;
		m_pointLightThreeColour.g = glm::sin(time * 2.66f * m_timeModifier) + 1.0f;
		m_pointLightThreeColour.b = glm::sin(time * 2.48f * m_timeModifier) + 1.0f;
	}

	//Cycle sunlight colour
	if (m_auroraMode)
	{
		m_sunlightColour.r = glm::sin(time * 1.0f * m_timeModifier) + 1.0f;
		m_sunlightColour.g = glm::sin(time * 0.5f * m_timeModifier) + 1.0f;
		m_sunlightColour.b = glm::sin(time * 0.6f * m_timeModifier) + 1.0f;
	}

	//Move point lights in circles
	if (m_fairyMode)
	{
		if (!m_pointLightsMoving)
		{
			m_pointLightOneStartPos = m_pointLightOnePosition;
			m_pointLightTwoStartPos = m_pointLightTwoPosition;
			m_pointLightThreeStartPos = m_pointLightThreePosition;
			m_pointLightsMoving = true;
		}
	
		m_pointLightOnePosition.x = m_pointLightOneStartPos.x + glm::sin(time * m_timeModifier);
		m_pointLightOnePosition.y = m_pointLightOneStartPos.y + glm::cos(time * m_timeModifier);
		m_pointLightTwoPosition.x = m_pointLightTwoStartPos.x + glm::sin(time * m_timeModifier);
		m_pointLightTwoPosition.y = m_pointLightTwoStartPos.y + glm::cos(time * m_timeModifier);
		m_pointLightThreePosition.x = m_pointLightThreeStartPos.x + glm::cos(time * m_timeModifier);
		m_pointLightThreePosition.y = m_pointLightThreeStartPos.y + glm::sin(time * m_timeModifier);
	}
	else
	{
		m_pointLightsMoving = false;
	}
}

void GraphicsApp::draw()
{
	//Wipe the screen to the background colour
	clearScreen();

	//Bind camera transform
	glm::mat4 projectionMatrix = m_camera.getProjectionMatrix((float)getWindowWidth(), (float)getWindowHeight());
	glm::mat4 viewMatrix = m_camera.getViewMatrix();

	//Draw instances (meshes) in the scene
	m_scene->draw();

	//Draw 3D gizmos (reference grid, point light spheres)
	Gizmos::draw(projectionMatrix * viewMatrix);

	//Draw 2D gizmos using an orthogonal projection matrix (or screen dimensions)
	//Gizmos::draw2D((float)getWindowWidth(), (float)getWindowHeight());
}