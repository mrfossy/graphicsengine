#pragma once
#include "glm/ext.hpp"

using glm::vec3;
using glm::mat4;

class Camera
{
private:

	float m_theta;												//camera's x-axis
	float m_phi;												//camera's y-axis
	glm::vec3 m_position;
	float m_fov;												//camra's FOV; increasing this gives a wider FOV; decreasing narrows FOV;
	float m_lastMouseX;											//stores mouse X position from previous frame
	float m_lastMouseY;											//stores mouse Y position from previous frame
	//float m_turnSpeed;										//mouse turning speed
	//bool m_invertY;											//mouse Y inversion
	float m_moveSpeed;											//camera tracking speed

public:

	Camera();
	Camera(float fov);											//Constructor with custom field-of-view

	void Camera::update(float deltaTime);
	glm::mat4 Camera::getViewMatrix();							//Assemble the camera view matrix
	glm::mat4 Camera::getProjectionMatrix(float w, float h);	//Assemble the camera projection matrix
	glm::vec3 getPosition() { return m_position; }
};