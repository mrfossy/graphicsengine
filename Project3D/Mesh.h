#pragma once
#include "glm/vec4.hpp"
#include"glm/vec2.hpp"

class Mesh
{
public:

	Mesh() : triCount(0), vao(0), vbo(0), ibo(0) {}		//0 represents 'uninitialized' in OpenGL
	virtual ~Mesh();									//cleans up any initialized buffers

	struct Vertex
	{
		glm::vec4 position;
		glm::vec4 normal;								//surface facing direction
		glm::vec2 texCoord;								//texture coordinate
	};

	void initializeQuad();
	
	//Takes an array of Vertex objects, integer count of how many vertices there are, an array of unsigned int indices, and a count of indices.
	//(Since you don't need indices for a mesh, these can be optional parameters.)
	void initialize(unsigned int vertexCount, const Vertex* vertices, unsigned int indexCount = 0, unsigned int* indices = nullptr);
	virtual void draw();								//virtual because you may want to derive from Mesh and override Draw() for custom drawing

protected:

	unsigned int triCount;
	unsigned int vao, vbo, ibo;							//Vertex Array Object, Vertex Buffer Object, Index Buffer Object

	//VAO is a sort of wrapper for the mesh, containing VBOs and an IBO, along with 
	//properties specifying the layout of the vertex buffers and how they map
	//to the input of the shader. The VAO, VBOs ad IBO are all unsigned integers,
	//acting as handles to the data stored on the GPU.
};