#pragma once

#include "Application.h"
#include <glm/mat4x4.hpp>
#include "Shader.h"
#include "Mesh.h"
#include "OBJMesh.h"
#include "Camera.h"
#include "Instance.h"
#include "Light.h"

class GraphicsApp : public aie::Application
{
public:

	GraphicsApp();
	virtual ~GraphicsApp();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

protected:

	Scene* m_scene;
	Camera m_camera;
	Light m_sunlight;

	//Light properties
	glm::vec3 m_sunlightDirection;
	glm::vec3 m_sunlightColour;

	glm::vec3 m_pointLightOnePosition;
	glm::vec3 m_pointLightOneColour;
	float m_pointLightOneIntensity;

	glm::vec3 m_pointLightTwoPosition;
	glm::vec3 m_pointLightTwoColour;
	float m_pointLightTwoIntensity;

	glm::vec3 m_pointLightThreePosition;
	glm::vec3 m_pointLightThreeColour;
	float m_pointLightThreeIntensity;

	//Shaders
	aie::ShaderProgram m_shader;
	aie::ShaderProgram m_phongShader;
	aie::ShaderProgram m_texturedShader;
	aie::ShaderProgram m_normalMapShader;

	//Meshes
	aie::OBJMesh m_fireHydrantMesh;
	aie::OBJMesh m_wallMesh;

	//Extra light properties
	bool m_argentoLightsMode = false;
	bool m_auroraMode = false;
	bool m_fairyMode = false;
	float m_timeModifier = 1.0f;
	bool m_pointLightsMoving = false;
	glm::vec3 m_pointLightOneStartPos = { 0, 0, 0 };
	glm::vec3 m_pointLightTwoStartPos = { 0, 0, 0 };
	glm::vec3 m_pointLightThreeStartPos = { 0, 0, 0 };
};