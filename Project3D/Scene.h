#pragma once
#include "glm/ext.hpp"
#include "Camera.h"
#include <list>
#include <vector>

using glm::vec2;
using glm::vec3;

class Instance;
class Light;
const int MAX_LIGHTS = 4;		//must match constant MAX_LIGHTS in normalmap.frag

class Scene
{
protected:

	Camera* m_camera;
	glm::vec2 m_windowSize;
	Light* m_sunLight;
	std::vector<Light*> m_pointLights;
	glm::vec3 m_pointLightPositions[MAX_LIGHTS];
	glm::vec3 m_pointLightColours[MAX_LIGHTS];
	glm::vec3 m_ambientLight;
	std::list<Instance*> m_instances;

public:
	~Scene();
	Scene(Camera* camera, glm::vec2 windowSize, Light* sunLight, glm::vec3 ambientLight);
	void addInstance(Instance* instance);
	void draw();
	Camera* getCamera() { return m_camera; }
	glm::vec2 getWindowSize() { return m_windowSize; }
	glm::vec3 getAmbientLight() { return m_ambientLight; }
	Light* getSunLight() { return m_sunLight; }
	int getNumLights() { return (int)m_pointLights.size(); }
	glm::vec3* getPointLightPositions() { return &m_pointLightPositions[0]; }	//returns block of 3 floats from memory beginning at index 0
	glm::vec3* getPointLightColours() { return &m_pointLightColours[0]; }	
	std::vector<Light*>& getPointLights() { return m_pointLights; }
	void addPointLight(glm::vec3 position, glm::vec3 colour, float intensity);
};