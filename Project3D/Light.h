#pragma once
#include "glm/ext.hpp"

using glm::vec3;

class Light
{
protected:

	glm::vec3 m_direction;
	glm::vec3 m_colour;

public:

	Light();
	Light(glm::vec3 direction, glm::vec3 colour);					//directional light
	Light(glm::vec3 position, glm::vec3 colour, float intensity);	//point light

	void setDirection(glm::vec3 direction) { m_direction = direction; }
	void setColour(glm::vec3 colour) { m_colour = colour; }
	glm::vec3 getColour() { return m_colour; }
	glm::vec3 getDirection() { return m_direction; }
};