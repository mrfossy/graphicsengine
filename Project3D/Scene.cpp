#include "Scene.h"
#include "Instance.h"

Scene::~Scene()
{
	//Loop over each instance and delete them
	for (auto it = m_instances.begin(); it != m_instances.end(); it++)
	{
		delete* it;
	}

	//Delete lights
	for (auto it = m_pointLights.begin(); it != m_pointLights.end(); it++)
	{
		delete* it;
	}
}

Scene::Scene(Camera* camera, glm::vec2 windowSize, Light* sunLight, glm::vec3 ambientLight)
{
	m_camera = camera;
	m_windowSize = windowSize;
	m_sunLight = sunLight;
	m_ambientLight = ambientLight;
	m_pointLightColours[0] = vec3();
	m_pointLightPositions[0] = vec3();
}

//Add an instance to the scene list
void Scene::addInstance(Instance* instance)
{
	m_instances.push_back(instance);
}

//Draw instances in the scene
void Scene::draw()
{
	//Fill in point light position/colour arrays
	for (int i = 0; i < MAX_LIGHTS && i < m_pointLights.size(); i++)
	{
		m_pointLightPositions[i] = m_pointLights[i]->getDirection();
		m_pointLightColours[i] = m_pointLights[i]->getColour();
	}
	
	//Loop over each instance and draw them in order
	for (auto it = m_instances.begin(); it != m_instances.end(); it++)
	{
		Instance* instance = *it;
		instance->draw(this);
	}
}

//Add a point light to the scenes pointLights vector
void Scene::addPointLight(glm::vec3 position, glm::vec3 colour, float intensity)
{
	Light* light = new Light(position, colour, intensity);
	m_pointLights.push_back(light);
}