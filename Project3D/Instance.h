#pragma once
#include "glm/ext.hpp"
#include "OBJMesh.h"
#include "Shader.h"
#include "Camera.h"
#include "Light.h"

using glm::vec3;
using glm::mat4;

class Scene;

class Instance
{
protected:
	glm::mat4 m_transform;
	aie::OBJMesh* m_mesh;
	aie::ShaderProgram* m_shader;
	glm::vec3 m_position;
	glm::vec3 m_eulerAngles;
	glm::vec3 m_scale;
	aie::Texture* m_diffuseTexture;
	aie::Texture* m_specularTexture;
	aie::Texture* m_normalTexture;

public:
	Instance(glm::mat4 transform, aie::OBJMesh* mesh, aie::ShaderProgram* shader);
	Instance(glm::mat4 transform, aie::OBJMesh* mesh, aie::ShaderProgram* shader, glm::vec3 position, glm::vec3 eulerAngles, glm::vec3 scale);
	Instance(glm::mat4 transform, aie::OBJMesh* mesh, aie::ShaderProgram* shader, glm::vec3 position, glm::vec3 eulerAngles, glm::vec3 scale, aie::Texture* diffuseTexture, aie::Texture* specularTexture, aie::Texture* normalTexture);

	void draw(Scene* scene);
	glm::vec3 getPosition() { return m_position; }
	void setPosition(glm::vec3 position) { m_position = position; }
	glm::vec3 getEulerAngles() { return m_eulerAngles; }
	void setEulerAngles(glm::vec3 eulerAngles) { m_eulerAngles = eulerAngles; }
	glm::vec3 getScale() { return m_scale; }
	void setScale(glm::vec3 scale) { m_scale = scale; }
	glm::mat4 makeTransform(glm::vec3 position, glm::vec3 eulerAngles, glm::vec3 scale);
};