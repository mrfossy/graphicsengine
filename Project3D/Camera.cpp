#include "Camera.h"
#include "Input.h"

Camera::Camera()
{
	m_position = { 0, 4, -20 };
	m_theta = 90;
	m_phi = 0;
	m_lastMouseX = 0;
	m_lastMouseY = 0;
	m_fov = 0.25f;		//default 90-degree (PI / 4) FOV
	m_moveSpeed = 8.0f;
}

//Constructor with custom fied-of-view
Camera::Camera(float fov)
{
	m_position = { 0, 4, -20 };
	m_theta = 90;
	m_phi = 0;
	m_lastMouseX = 0;
	m_lastMouseY = 0;
	m_fov = fov;
	m_moveSpeed = 8.0f;
}

void Camera::update(float deltaTime)
{
	aie::Input* input = aie::Input::getInstance();
	float thetaR = glm::radians(m_theta);
	float phiR = glm::radians(m_phi);

	//Calculate the forwards, right and up axes for the camera
	glm::vec3 forward(cos(phiR) * cos(thetaR), sin(phiR), cos(phiR) * sin(thetaR));
	glm::vec3 right(-sin(thetaR), 0, cos(thetaR));
	glm::vec3 up(0, 1, 0);

	///Use WASD and ZX keys to move the camera
	if (input->isKeyDown(aie::INPUT_KEY_X) || input->isKeyDown(aie::INPUT_KEY_PAGE_UP))
		m_position += up * deltaTime * m_moveSpeed;

	if (input->isKeyDown(aie::INPUT_KEY_Z) || input->isKeyDown(aie::INPUT_KEY_PAGE_DOWN))
		m_position -= up * deltaTime * m_moveSpeed;

	///Use WASD and ZX keys to move the camera
	if (input->isKeyDown(aie::INPUT_KEY_W) || input->isKeyDown(aie::INPUT_KEY_UP))
		m_position += forward * deltaTime * m_moveSpeed;

	///Use WASD and ZX keys to move the camera
	if (input->isKeyDown(aie::INPUT_KEY_A) || input->isKeyDown(aie::INPUT_KEY_LEFT))
		m_position -= right * deltaTime * m_moveSpeed;

	///Use WASD and ZX keys to move the camera
	if (input->isKeyDown(aie::INPUT_KEY_S) || input->isKeyDown(aie::INPUT_KEY_DOWN))
		m_position -= forward * deltaTime * m_moveSpeed;

	///Use WASD and ZX keys to move the camera
	if (input->isKeyDown(aie::INPUT_KEY_D) || input->isKeyDown(aie::INPUT_KEY_RIGHT))
		m_position += right * deltaTime * m_moveSpeed;

	//Get the current mouse coordinates
	float mx = (float)input->getMouseX();
	float my = (float)input->getMouseY();
	const float turnSpeed = 0.1f;
	//If the right mouse button is down, increment theta and phi
	if (input->isMouseButtonDown(aie::INPUT_MOUSE_BUTTON_RIGHT))
	{
		m_theta += turnSpeed * (mx - m_lastMouseX);
		m_phi -= turnSpeed * (my - m_lastMouseY);	//inverted Y; this could be an option in the constructor
		glm::clamp(m_phi, -70.0f, 70.0f);			//clamp camera Y-axis to prevent gimbal lock
	}

	//Store this frame's values for next frame
	m_lastMouseX = mx;
	m_lastMouseY = my;
}

//Assemble the camera view matrix
glm::mat4 Camera::getViewMatrix()
{
	//Convert thetaR and phiR to radians; find the camera's forward vector (using trigonometry)
	float thetaR = glm::radians(m_theta);
	float phiR = glm::radians(m_phi);
	glm::vec3 forward(cos(phiR) * cos(thetaR), sin(phiR), cos(phiR) * sin(thetaR));

	//glm::lookAt constructs a view matrix from a position, another position that we're looking at, and an up vector.
	//It follows the process:
	//1. Cross product the forward vector with the world up vector and normalize to get the camera's x-axis
	//2. Cross product the z-axis and x-axis to the get the y-axis (both are unit vectors, so you don't need to normalize in this step)
	return glm::lookAt(m_position, m_position + forward, glm::vec3(0, 1, 0));
}

//Assemble the camera perspective matrix
glm::mat4 Camera::getProjectionMatrix(float w, float h)
{
	//Calculate from the field of view, screen width and height for aspect ratio, and near and far plane values
	float fieldOfView = glm::pi<float>() * m_fov;
	float aspectRatio = w / h;
	float nearPlane = 0.1f;
	float farPlane = 1000.0f;
	return glm::perspective(fieldOfView, aspectRatio, nearPlane, farPlane);
}