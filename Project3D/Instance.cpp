#include "Instance.h"
#include "Scene.h"

Instance::Instance(glm::mat4 transform, aie::OBJMesh* mesh, aie::ShaderProgram* shader)
{
	m_transform = transform;
	m_mesh = mesh;
	m_shader = shader;
	m_position = { 0, 0, 0 };
	m_eulerAngles = { 0, 0, 0 };
	m_scale = { 1, 1, 1 };
	m_diffuseTexture = nullptr;
	m_specularTexture = nullptr;
	m_normalTexture = nullptr;
	m_transform = makeTransform(m_position, m_eulerAngles, m_scale);
}

//Constructor takes in position/rotation/scale; used for textured OBJ meshes
Instance::Instance(glm::mat4 transform, aie::OBJMesh* mesh, aie::ShaderProgram* shader, glm::vec3 position, glm::vec3 eulerAngles, glm::vec3 scale)
{
	m_transform = transform;
	m_mesh = mesh;
	m_shader = shader;
	m_position = position;
	m_eulerAngles = eulerAngles;
	m_scale = scale;
	m_diffuseTexture = nullptr;
	m_specularTexture = nullptr;
	m_normalTexture = nullptr;
	m_transform = makeTransform(m_position, m_eulerAngles, m_scale);
}

//Constructor takes in diffuse/specular/normal textures; used for un-textured OBJ meshes
Instance::Instance(glm::mat4 transform, aie::OBJMesh* mesh, aie::ShaderProgram* shader, glm::vec3 position, glm::vec3 eulerAngles, glm::vec3 scale, aie::Texture* diffuseTexture, aie::Texture* specularTexture, aie::Texture* normalTexture)
{
	m_transform = transform;
	m_mesh = mesh;
	m_shader = shader;
	m_position = position;
	m_eulerAngles = eulerAngles;
	m_scale = scale;
	m_diffuseTexture = diffuseTexture;
	m_specularTexture = specularTexture;
	m_normalTexture = normalTexture;
	m_transform = makeTransform(m_position, m_eulerAngles, m_scale);
}

//Binds shader, uniforms; draws the instance
void Instance::draw(Scene* scene)
{
	//Set the shader pipeline
	m_shader->bind();

	//Bind the uniform for the ProjectionViewModel transform (combination of projection, view and model transform)
	auto pvm = scene->getCamera()->getProjectionMatrix(scene->getWindowSize().x, scene->getWindowSize().y) * scene->getCamera()->getViewMatrix() * m_transform;
	m_shader->bindUniform("ProjectionViewModel", pvm);

	//Bind transforms for lighting
	m_shader->bindUniform("ModelMatrix", m_transform);
	m_shader->bindUniform("AmbientColour", scene->getAmbientLight());
	m_shader->bindUniform("LightColour", scene->getSunLight()->getColour());
	m_shader->bindUniform("LightDirection", scene->getSunLight()->getDirection());
	m_shader->bindUniform("cameraPosition", scene->getCamera()->getPosition());

	int numLights = scene->getNumLights();
	m_shader->bindUniform("numLights", numLights);
	m_shader->bindUniform("PointLightPosition", numLights, scene->getPointLightPositions());
	m_shader->bindUniform("PointLightColour", numLights, scene->getPointLightColours());

	m_mesh->draw();
}

//Calculate transform matrix by concatenating each operation in the right order (suboptimal)
glm::mat4 Instance::makeTransform(glm::vec3 position, glm::vec3 eulerAngles, glm::vec3 scale)
{
	return glm::translate(glm::mat4(1), position)
		* glm::rotate(glm::mat4(1), glm::radians(eulerAngles.z), glm::vec3(0, 0, 1))
		* glm::rotate(glm::mat4(1), glm::radians(eulerAngles.y), glm::vec3(0, 1, 0))
		* glm::rotate(glm::mat4(1), glm::radians(eulerAngles.x), glm::vec3(1, 0, 0))
		* glm::scale(glm::mat4(1), scale);
}